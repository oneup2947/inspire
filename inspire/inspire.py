#!/usr/bin/env python3

import json
import random
import os


class bcolors:
    TEXT = '\033[94m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    ENDC = '\033[0m'

def generateQuote():
    abspath = '/usr/local'
    path = os.path.join(abspath, 'share', 'inspire', 'data')
    data = json.load(open(path+"/"+'data.json'))
    randomNumber = random.randint(0,len(data['data']))

    print("\n")
    print(bcolors.BOLD +  bcolors.TEXT + data['data'][randomNumber]['quote'])
    print("-" + data['data'][randomNumber]['author'] + bcolors.ENDC)
    print("\n")
generateQuote()
