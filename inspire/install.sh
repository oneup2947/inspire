#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be with sudo" 1>&2
   exit 1
fi

INSTALLDIR="/usr/local/share"

# Create inspire folder
mkdir -p $INSTALLDIR/inspire

# Symlink the json datafolder
ln -sf $PWD/data/ $INSTALLDIR/inspire/data


# Copy the executable
cp inspire.py /usr/local/bin/inspire
