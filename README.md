A small application to generate random motivational quote from Terminal in Linux/Mac.

Steps to SetUp:

$ git clone
$ cd inspire/inspire
$ sudo ./install.sh

Author: Vishal Kumar
Contact: vishal.2947@gmail.com
